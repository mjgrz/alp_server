let cookieParser = require("cookie-parser");
let bodyParser = require("body-parser");
let express = require("express");
let cors = require("cors");
const app = express();
require("dotenv/config");
const jwt = require("jsonwebtoken");

app.use(express.json());
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(cookieParser());
app.use(bodyParser.json({ limit: "25mb" }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

const auth = (req, res, next) => {
  if (req.session){
    if(req.session.userid){
      return next();
    } else {
      return res.status(401).send("*********** Unauthorized Access ***********");
    }
  } else {
    return res.status(401).send("*********** Unauthorized Access ***********");
  }
};

const authenticateToken = (req, res, next) => {
  const token = req.cookies.jwt;
  if (!token) {
    return res
      .status(401)
      .json({ message: "*********** Unauthorized Access ***********" });
  }
  try {
    const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
    if (req.session.email === decoded.email) {
      next();
    } else {
      return res.status(403).json({ message: "Invalid token." });
    }
  } catch (error) {
    return res.status(403).json({ message: "Invalid token." });
  }
};

module.exports = {
  auth,
  authenticateToken,
};
