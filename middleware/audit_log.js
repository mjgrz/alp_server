const Pool = require("pg").Pool;
const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const { encrypt } = require("../fx_queries/encryptorFunction");

require("dotenv/config");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const sociocon = new Pool({
    user: process.env.SEP_USER,
    host: process.env.SEP_HOST,
    database: process.env.SEP_DB,
    password: process.env.SEP_PASS,
    port: process.env.SEP_PORT,
});
  
sociocon.connect(function (err) {
    if (err) throw err;
    console.log("SocioEco-Audit Log Database connected successfully");
})
// CONFIGURATION

const log_post = (req, res) => {
  const ACT = req.body.act;
  const UID = req.body.userid;
  const DATE = req.body.date;
  const TIME = req.body.time;
  sociocon.query(`INSERT INTO public.socio_tblx_bene_log 
  (log_activity, log_user_id, log_date, log_time) 
  VALUES($1, $2, $3, $4)`, 
  [ACT, UID, DATE, TIME], 
  function(err, results, fields){
      if (err) {
          console.log(err);
          res.json('Failed');
      } 
      else {
          res.json('Success');
      }
  });
};

// AUDIT TRAIL DATA FETCH
const xlog_count = (req, res) => {
  const DT1 = req.body.dt1;
  const DT2 = req.body.dt2;
  sociocon.query(`SELECT COUNT(*) FROM public.socio_log_view 
  WHERE log_date::date >= $1::date AND log_date::date <= $2::date`, 
  [DT1, DT2], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};

const xlog_countlike = (req, res) => {
  const SEARCH = '%'+req.body.search+'%';
  const DT1 = req.body.dt1;
  const DT2 = req.body.dt2;
  sociocon.query(`SELECT COUNT(*) FROM public.socio_log_view 
  WHERE (u_mail = $1 OR lname ILIKE $1 OR fname ILIKE $1 
  OR mname ILIKE $1 OR region ILIKE $1 OR district ILIKE $1) 
  AND (log_date::date >= $2::date AND log_date::date <= $3::date)`, 
  [SEARCH, DT1, DT2], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};

const xlog_set1 = (req, res) => {
  const OFFSET = req.body.offset;
  const LIMIT = req.body.limit;
  const DT1 = req.body.dt1;
  const DT2 = req.body.dt2;
  sociocon.query(`SELECT * FROM public.socio_log_view 
  WHERE log_date::date >= $1::date AND log_date::date <= $2::date 
  OFFSET $3 LIMIT $4`, 
  [DT1, DT2, OFFSET, LIMIT], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};

const xlog_set2 = (req, res) => {
  const OFFSET = req.body.offset;
  const LIMIT = req.body.limit;
  const SEARCH = '%'+req.body.search+'%';
  const DT1 = req.body.dt1;
  const DT2 = req.body.dt2;
  sociocon.query(`SELECT * FROM public.socio_log_view 
  WHERE (u_mail = $1 OR lname ILIKE $1 OR fname ILIKE $1 
  OR mname ILIKE $1 OR region ILIKE $1 OR district ILIKE $1) 
  AND (log_date::date >= $2::date AND log_date::date <= $3::date) 
  OFFSET $4 LIMIT $5`, 
  [SEARCH, DT1, DT2, OFFSET, LIMIT], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// AUDIT TRAIL DATA FETCH

module.exports = {
    log_post,
    xlog_count,
    xlog_countlike,
    xlog_set1,
    xlog_set2,
};