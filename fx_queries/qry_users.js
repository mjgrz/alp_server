const express = require('express')
const app = express()
const Pool = require("pg").Pool;
require('dotenv/config');

const bodyParser = require('body-parser');
const { encrypt } = require("./encryptorFunction");

const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const userscon = new Pool({
    user: process.env.SDT_USER,
    host: process.env.SDT_HOST,
    database: process.env.SDT_DB,
    password: process.env.SDT_PASS,
    port: process.env.SDT_PORT,
});
  
userscon.connect(function (err) {
    if (err) throw err;
    console.log("Users connected successfully");
})
// CONFIGURATION

// USER DATA 
const alphaListingUsers = (req, res) => {
  userscon.query(`SELECT * FROM public.tblx_users_view`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// USER DATA 

// INSERT USER DATA
const insertUser = (req, res) => {
  const USERID = req.body.userid
  const LNAME = req.body.lname
  const FNAME = req.body.fname
  const MNAME = req.body.mname
  const EMAIL = req.body.email
  const EMPNO = req.body.empno
  const ACCESS = req.body.access
  const REGION = req.body.region
  const DISTRICT = req.body.district
  const PASSWORD = req.body.password
  userscon.query(`INSERT INTO public.tblx_users
  (user_id, user_lname, user_fname, user_mname, user_email, 
  user_empno, user_access, user_region, 
  user_district, user_password) 
  VALUES 
  ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`, 
  [USERID, LNAME, FNAME, MNAME, EMAIL, EMPNO, ACCESS, 
  REGION, DISTRICT, PASSWORD], function(err, results, fields){
    if (err) {
      console.log(err);
      res.json('Failed');
    } else {
      res.json('Success');
    }
  });
};
// INSERT USER DATA

// UPDATE USER DATA
const updateUser = (req, res) => {
  const USERID = req.body.userid
  const LNAME = req.body.lname
  const FNAME = req.body.fname
  const MNAME = req.body.mname
  const EMAIL = req.body.email
  const EMPNO = req.body.empno
  const ACCESS = req.body.access
  const REGION = req.body.region
  const DISTRICT = req.body.district
  const STATUS = req.body.status
  userscon.query(`UPDATE public.tblx_users SET 
  user_lname = $2, user_fname = $3, user_mname = $4, 
  user_email = $5, user_empno = $6, user_access = $7, 
  user_region = $8, user_district = $9, 
  user_status = $10 
  WHERE user_id = $1`, 
  [USERID, LNAME, FNAME, MNAME, EMAIL, EMPNO, ACCESS, 
  REGION, DISTRICT, STATUS], function(err, results, fields){
    if (err) {
      console.log(err);
      res.json('Failed');
    } else {
      res.json('Success');
    }
  });
};
// UPDATE USER DATA

// UPDATE USER EMAIL
const updateEmail = (req, res) => {
  const USERID = req.body.userid
  const EMAIL = req.body.email
  userscon.query(`UPDATE public.tblx_users SET 
  user_email = $2 WHERE user_id = $1`, 
  [USERID, EMAIL], function(err, results, fields){
    if (err) {
      console.log(err);
      res.json('Failed');
    } else {
      res.json('Success');
    }
  });
};
// UPDATE USER EMAIL

// UPDATE USER PASSWORD
const updatePass = (req, res) => {
  const USERID = req.body.userid
  const PASS = req.body.password
  userscon.query(`UPDATE public.tblx_users SET 
  user_password = $2 WHERE user_id = $1`, 
  [USERID, PASS], function(err, results, fields){
    if (err) {
      console.log(err);
      res.json('Failed');
    } else {
      res.json('Success');
    }
  });
};
// UPDATE USER PASSWORD

module.exports = {
    alphaListingUsers,
    insertUser,
    updateUser,
    updateEmail,
    updatePass,
};