const express = require('express')
const app = express()
const Pool = require("pg").Pool;
require('dotenv/config');

const bodyParser = require('body-parser');
const { encrypt } = require("./encryptorFunction");

const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const rsrccon = new Pool({
    user: process.env.FDW_USER,
    host: process.env.FDW_HOST,
    database: process.env.FDW_DB,
    password: process.env.FDW_PASS,
    port: process.env.FDW_PORT,
});
  
rsrccon.connect(function (err) {
    if (err) throw err;
    console.log("Resources connected successfully");
})
// CONFIGURATION

// PROJECT DETAILS 
const projDetails = (req, res) => {
    rsrccon.query(`SELECT "BP_PR_NAME" AS proj_name, 
    "BP_PR_DO" AS proj_do, "BP_PR_RO" AS proj_ro, 
    "BP_PR_CODE" AS proj_code, "BP_PR_NAMED" AS proj_named 
    FROM public.bp_projdetails`, 
    [], function(err, results, fields){
    if (err) throw err;
    const encrypted = encrypt(results);
    res.status(200).json({ encrypted });
    });
};
// PROJECT DETAILS 

// REGION OFFICES 
const regOffc = (req, res) => {
  rsrccon.query(`SELECT "BP_ID" AS ro_id, 
  "BP_REGION" AS ro_name 
  FROM public.bp_regionoffice`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// REGION OFFICES 

// DISTRICT OFFICES 
const distOffc = (req, res) => {
  rsrccon.query(`SELECT "BP_DO_ID" AS do_id, 
  "BP_DO_OFFICE" AS do_name, "BP_ROID" AS ro_id 
  FROM public.bp_districtoffice`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// DISTRICT OFFICES 

module.exports = {
  projDetails,
  regOffc,
  distOffc,
};