const express = require('express')
const app = express()
const Pool = require("pg").Pool;
require('dotenv/config');

const bodyParser = require('body-parser');
const { encrypt } = require("./encryptorFunction");

const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const fdwcon = new Pool({
    user: process.env.FDW_USER,
    host: process.env.FDW_HOST,
    database: process.env.FDW_DB,
    password: process.env.FDW_PASS,
    port: process.env.FDW_PORT,
});
  
fdwcon.connect(function (err) {
    if (err) throw err;
    console.log("FDW connected successfully");
})
// CONFIGURATION

// SEARCH NAME BCS20 
const searchBCS20 = (req, res) => {
    const FIRSTNAME = `%${req.body.firstname}%`;
    const LASTNAME = `%${req.body.lastname}%`;
    const MIDDLENAME = `%${req.body.middlename}%`;
    fdwcon.query(`SELECT a.bin, a.ro_id, a.pj_name, a.ro_region, 
    a.loc_desc, a.com_code, a.lname, a.fname, 
    a.mname, a.bdate, SUM(a.act_bal) AS baltotal, MAX(a.monpdto) AS monpdto 
    FROM (SELECT com_code, lname, fname, mname, bdate, 
    bcs20.bin, act_bal, monpdto, pj_name, loc_desc, ro_id, ro_region FROM 
    public.gis_tblx_bcs0020_bcs AS bcs20 
    LEFT JOIN public.gis_tblx_bcs0030 AS bcs30 ON 
    bcs30.bin = bcs20.bin 
    LEFT JOIN public.alp_proj_loclib AS proj ON 
    proj.pj_code = SUBSTRING(bcs20.com_code,0,4) 
    WHERE fname ILIKE $1 AND lname ILIKE $2 AND mname ILIKE $3) a 
    GROUP BY a.bin, a.ro_id, a.pj_name, a.ro_region, a.loc_desc,  
    a.com_code, a.lname, a.fname, a.mname, a.bdate`, 
    [FIRSTNAME, LASTNAME, MIDDLENAME], function(err, results, fields){
    if (err) throw err;
    const encrypted = encrypt(results);
    res.status(200).json({ encrypted });
    });
};
// SEARCH NAME BCS20 

// SEARCH NAME LIS20 
const searchLISBCS20 = (req, res) => {
    const FIRSTNAME = `%${req.body.firstname}%`;
    const LASTNAME = `%${req.body.lastname}%`;
    const MIDDLENAME = `%${req.body.middlename}%`;
    fdwcon.query(`SELECT * FROM public.gis_tblx_lis20 AS lis 
    LEFT JOIN public.alp_proj_loclib AS proj ON 
    proj.pj_code = SUBSTRING(lis.com_code,0,4) 
    WHERE fname ILIKE $1 AND lname ILIKE $2 AND mname ILIKE $3`, 
    [FIRSTNAME, LASTNAME, MIDDLENAME], function(err, results, fields){
    if (err) throw err;
    const encrypted = encrypt(results);
    res.status(200).json({ encrypted });
    });
};
// SEARCH NAME LIS20 

// SEARCH NAME BCS20 BATCH
const searchBCS20batch = (req, res) => {
  const ARRAY = req.body.array;
  const QRY = `SELECT a.bin, a.ro_id, a.pj_name, a.ro_region, 
  a.loc_desc, a.com_code, a.lname, a.fname, 
  a.mname, a.bdate, SUM(a.act_bal) AS baltotal, MAX(a.monpdto) AS monpdto 
  FROM (SELECT com_code, lname, fname, mname, bdate, 
  bcs20.bin, act_bal, monpdto, pj_name, loc_desc, ro_id, ro_region FROM 
  public.gis_tblx_bcs0020_bcs AS bcs20 
  LEFT JOIN public.gis_tblx_bcs0030 AS bcs30 ON 
  bcs30.bin = bcs20.bin 
  LEFT JOIN public.alp_proj_loclib AS proj ON 
  proj.pj_code = SUBSTRING(bcs20.com_code,0,4) 
  WHERE (fname, lname, mname) 
  IN ${ARRAY} ) a 
  GROUP BY a.bin, a.ro_id, a.pj_name, a.ro_region, a.loc_desc,  
  a.com_code, a.lname, a.fname, a.mname, a.bdate 
  ORDER BY a.lname`;

  // const QRY = `SELECT * FROM 
  // public.gis_tblx_bcs0020_bcs AS bcs20 
  // LEFT JOIN public.alp_proj_loclib AS proj ON 
  // proj.pj_code = SUBSTRING(bcs20.com_code,0,4) 
  // WHERE (fname, lname, mname) 
  // IN ${ARRAY}`;

  fdwcon.query(QRY, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// SEARCH NAME BCS20 BATCH

// SEARCH NAME LIS20 BATCH
const searchLISBCS20batch = (req, res) => {
  const ARRAY = req.body.array;
  fdwcon.query(`SELECT * FROM public.gis_tblx_lis20 AS lis 
  LEFT JOIN public.alp_proj_loclib AS proj ON 
  proj.pj_code = SUBSTRING(lis.com_code,0,4) 
  WHERE (fname, lname, mname) 
  IN ${ARRAY} 
  ORDER BY lname`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// SEARCH NAME LIS20 BATCH

// SEARCH NAME LIS20 BATCH
const searchSubHist = (req, res) => {
  const CODE = req.body.com_code;
  fdwcon.query(`SELECT * FROM public.bcs_tblx_subhist 
  WHERE "COM_CODE" = $1`, 
  [CODE], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// SEARCH NAME LIS20 BATCH

// SEARCH NAME BCS20 STEP 1
const srchBCS1 = (req, res) => {
  const ARRAY = req.body.array;
  const QRY = `SELECT bin, lname, mname, fname, bdate, com_code 
  FROM public.gis_tblx_bcs0020_bcs 
  WHERE (fname, lname, mname) 
  IN ${ARRAY} 
  ORDER BY bin`;

  fdwcon.query(QRY, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// SEARCH NAME BCS20 STEP 1

// SEARCH NAME BCS20 STEP 2
const srchBCS2 = (req, res) => {
  const ARRAY = req.body.array;
  const QRY = `SELECT bin, pj_name, 
  loc_desc, ro_id, ro_region, 
  SUM(act_bal) AS baltotal, MAX(monpdto) AS monpdto
  FROM (SELECT * FROM public.gis_tblx_bcs0030
  WHERE (bin) IN ${ARRAY}) bcsfinal 
  LEFT JOIN public.alp_proj_loclib AS proj ON 
  proj.pj_code = SUBSTRING(bcsfinal.bin,0,4) 
  GROUP BY bin, pj_name, loc_desc, ro_id, ro_region 
  ORDER BY bin`;
  fdwcon.query(QRY, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// SEARCH NAME BCS20 STEP 2

module.exports = {
  searchBCS20,
  searchLISBCS20,
  searchBCS20batch,
  searchLISBCS20batch,
  searchSubHist,
  srchBCS1,
  srchBCS2,
};