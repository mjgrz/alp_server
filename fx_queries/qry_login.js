const express = require('express')
const app = express()
const Pool = require("pg").Pool;
require('dotenv/config');
const jwt = require("jsonwebtoken");

const bodyParser = require('body-parser');
const { encrypt } = require("./encryptorFunction");

const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const nodemailer = require("nodemailer");

let transporter;
let mailOptions;

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const logincon = new Pool({
    user: process.env.SDT_USER,
    host: process.env.SDT_HOST,
    database: process.env.SDT_DB,
    password: process.env.SDT_PASS,
    port: process.env.SDT_PORT,
});
  
logincon.connect(function (err) {
    if (err) throw err;
    console.log("Login connected successfully");
})
// ----------------------------------------- CONFIGURATION -----------------------------------------

// ----------------------------------------- LOGIN POST -----------------------------------------
const loginpost = (req, res) => {
  const EMAIL = req.body.email
  logincon.query(`SELECT * FROM public.tblx_users 
  WHERE user_email = $1 AND user_status = 'Active'`, 
  [EMAIL], function(err, results, fields){
    if (err) throw err;
    const encrypted = encrypt(results);
    res.status(200).json({ encrypted });
  });
};
// ----------------------------------------- LOGIN POST -----------------------------------------

// ----------------------------------------- LOGIN CHECK -----------------------------------------
const loginsessionset = (req, res) => {
  const EMAIL = req.body.email
  logincon.query(`SELECT * FROM public.tblx_users_view 
  WHERE user_email = $1 AND user_status = 'Active'`, 
  [EMAIL], function(err, results, fields){
    if (results.rowCount >= 1) {
      req.session.userid = results.rows[0].user_id;
      req.session.lname = results.rows[0].user_lname;
      req.session.fname = results.rows[0].user_fname;
      req.session.mname = results.rows[0].user_mname;
      req.session.email = results.rows[0].user_email;
      req.session.empno = results.rows[0].user_empno;
      req.session.access = results.rows[0].user_access;
      req.session.region = results.rows[0].user_region;
      req.session.ro_desc = results.rows[0].ro_desc;
      req.session.district = results.rows[0].user_district;
      req.session.do_desc = results.rows[0].do_desc;
      req.session.password = results.rows[0].user_password;
      
      const accessToken = jwt.sign(
        { 
          usersID: results.rows[0].user_id, 
          email: EMAIL, 
          access: results.rows[0].user_access,
          district: results.rows[0].user_district,
        },
        process.env.TOKEN_SECRET,
        { expiresIn: oneDay }
      );

      res.status(200)
      .cookie("jwt", accessToken, {
        httpOnly: true,
      })
      .json({ response: "Ok" });
    } 
    else {
      res.status(401).json({ response: "Error Message: Information does not exist" });
    }
  });
};
// ----------------------------------------- LOGIN CHECK -----------------------------------------

// LOGOUT
const logoutpost = (req, res) => {
  req.session.destroy();
  res.status(200).json({ responsecode: "0934" });
};
// LOGOUT

// ----------------------------------------- CHECK SESSION -----------------------------------------
const checksession = (req, res) => {
  if (!req.session.email) {
    const encrypted = encrypt({ response: "notLoggedIn" });
    res.status(200).json({ encrypted });
  } 
  else {
    const encrypted = encrypt({ 
      response: "LoggedIn",
      fullname: req.session.lname + ", " + req.session.fname + " "+ req.session.mname,
      lastname: req.session.lname,
      firstname: req.session.fname,
      middlename: req.session.mname,
      userid: req.session.ident,
      email: req.session.email,
      region: req.session.region,
      ro_desc: req.session.ro_desc,
      district: req.session.district,
      do_desc: req.session.do_desc,
      empno: req.session.empno,
      access: req.session.access,
      id: req.session.userid,
      password: req.session.password,
    });
    res.status(200).json({ encrypted });
  }
};
// ----------------------------------------- CHECK SESSION -----------------------------------------

// ----------------------------------------- FORGOT PASSWORD FUNCTION -----------------------------------------
const forgotpass = (req, res) => {
  const EMPNO = req.body.empno;
  const EMAIL = req.body.email;
  logincon.query(`SELECT * FROM public.tblx_users 
  WHERE user_empno = $1 AND user_email = $2 
  AND user_status = 'Active'`, 
  [EMPNO, EMAIL], function(err, results, fields) {
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};

const resetpass = (req, res) => {
  const ID = req.body.id;
  const PASS = req.body.pass;
  const HASH = req.body.hash;
  const EMAIL = req.body.email;
  const FNAME = req.body.fname;
  logincon.query(`UPDATE public.tblx_users SET user_password = $1 
  WHERE user_id = $2`, 
  [HASH, ID], function(err, results, fields) {
  if (err) throw err;
  if (results.rowCount >= 1) {
    transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'no-reply@nha.gov.ph',
        pass: process.env.EMAIL_NOTIF,
      },
    });
    mailOptions = {
      from: 'no-reply@nha.gov.ph',
      to: EMAIL,
      subject: 'Your New Password and Login Link',
      html:
        `<!DOCTYPE html><html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><meta charset="UTF-8">` +
        `<meta name="viewport" content="width=device-width,initial-scale=1"><meta name="x-apple-disable-message-reformatting"><title></title>` +
        `</head><body>`+

        `<div style="width: 100%; text-align: center;">`+
            `<div>`+
                `<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/National_Housing_Authority_%28NHA%29.svg/1027px-National_Housing_Authority_%28NHA%29.svg.png" alt="" width="65" style="height:auto; display: flex; margin: auto; " />`+
                `<span style="font-size:25px;"><strong>Hello, ${FNAME}</strong></span>`+
            `</div>`+
            `<div>`+
                `<p>We've reset your password as requested. Here is your new password.</p>`+
                `<p style="font-size: 20px; color: #3f62be;">${PASS}</p>`+
                `<p>Access your account now: <a href="${process.env.ORIGIN_URL}">Remittance List Program</a></p>`+
                `<p>Remember to change your password after logging in for added security.</p>`+
            `</div>`+
        `</div>`+
        
        `</body></html>`,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
        res.json('Failed');
      } else {
        console.log('Email sent: ' + info.response);
        res.json('Success');
      }
    });
  } 
  if (results.rowCount < 1) {
    res.json('Failed');
  }
  });
};
// ----------------------------------------- FORGOT PASSWORD FUNCTION -----------------------------------------

module.exports = {
  loginpost,
  loginsessionset,
  logoutpost,
  checksession,
  forgotpass,
  resetpass,
};