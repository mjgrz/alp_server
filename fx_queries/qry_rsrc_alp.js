const express = require('express')
const app = express()
const Pool = require("pg").Pool;
require('dotenv/config');

const xlsx = require('xlsx');

const bodyParser = require('body-parser');
const { encrypt } = require("./encryptorFunction");

const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);

app.use(bodyParser.json({ limit: "25mb" }));
app.use(cors({ credentials: true, origin: process.env.ORIGIN_URL }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

const rsrccon = new Pool({
    user: process.env.SDT_USER,
    host: process.env.SDT_HOST,
    database: process.env.SDT_DB,
    password: process.env.SDT_PASS,
    port: process.env.SDT_PORT,
});
  
rsrccon.connect(function (err) {
    if (err) throw err;
    console.log("Resources-Users connected successfully");
})
// CONFIGURATION

// REGION OFFICES 
const usrRegOffc = (req, res) => {
  rsrccon.query(`SELECT * 
  FROM public.tblx_ro WHERE id != '1'`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// REGION OFFICES 

// DISTRICT OFFICES 
const usrDistOffc = (req, res) => {
  rsrccon.query(`SELECT * 
  FROM public.tblx_do WHERE id != '0'`, 
  [], function(err, results, fields){
  if (err) throw err;
  const encrypted = encrypt(results);
  res.status(200).json({ encrypted });
  });
};
// DISTRICT OFFICES 

// AFP EXCEL FILE AS DATABASE 
const afpDbase = (req, res) => {
  const SERIAL = req.body.array;

  const workbook = xlsx.readFile('fx_queries/afp-list-dbase.xlsx');
  let workbook_sheet = workbook.SheetNames;
  let workbook_response = xlsx.utils.sheet_to_json(workbook.Sheets[workbook_sheet[0]]);

  const RESULTS = workbook_response.filter(row => SERIAL.includes(row['SERIAL NO.']));

  const encrypted = encrypt(RESULTS);
  res.status(200).json({ encrypted });
};
// AFP EXCEL FILE AS DATABASE 

module.exports = {
  usrRegOffc,
  usrDistOffc,
  afpDbase,
};