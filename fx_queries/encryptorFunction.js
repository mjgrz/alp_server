const crypto = require("crypto");

const encrypt = (data) => {
  const ENCRYPTION_KEY_SEC = process.env.ENCRYPTION_KEY;
  const dataString = JSON.stringify(data);

  // Create an initialization vector (IV)
  const iv = crypto.randomBytes(16);

  // Create a cipher using AES-256-CBC algorithm
  const cipher = crypto.createCipheriv(
    "aes-256-cbc",
    Buffer.from(ENCRYPTION_KEY_SEC),
    iv
  );
  // Encrypt the data
  const encryptedData = Buffer.concat([
    cipher.update(dataString, "utf-8"),
    cipher.final(),
  ]);

  return {
    rxxid: iv.toString("hex"),
    bllprxx: encryptedData.toString("hex"),
  };
};

module.exports = { encrypt };
