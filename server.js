const cookieParser = require("cookie-parser");
const bodyParser = require('body-parser');
const express = require('express')
const app = express()
const Pool = require("pg").Pool;
const helmet = require("helmet");
const cors = require("cors");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);

require('dotenv/config');

const qry_fdw = require("./fx_queries/qry_fdw");
const qry_users = require("./fx_queries/qry_users");
const qry_rsrc = require("./fx_queries/qry_rsrc");
const qry_rsrc_alp = require("./fx_queries/qry_rsrc_alp");
const qry_login = require("./fx_queries/qry_login");
const sessioncheck = require("./middleware/session_check");

const store = new MongoDBStore({
  uri:
    "mongodb://" +
    process.env.MONGO_USERS +
    ":" +
    process.env.MONGO_PASSWORD +
    "@" +
    process.env.MONGO_HOST +
    "/" +
    process.env.MONGO_DB,
});

const corsOptions = {
  credentials: true,
  origin: process.env.ORIGIN_URL,
  methods: "GET, POST, PUT, DELETE",
  allowedHeaders: "Content-Type, Cache-Control",
};

app.disable("x-powered-by");
app.use(cors(corsOptions));
app.use(helmet.xssFilter());
app.use(helmet());
app.use(cookieParser());
app.use(bodyParser.json({ limit: "25mb" }));
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "25mb",
  })
);

const oneDay = 1000 * 60 * 60 * 24;
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    cookie: { maxAge: oneDay },
    resave: false,
    saveUninitialized: false,
    sameSite: "none",
    store: store,
  })
);
// ===================================== END OF COOKIE SESSION =====================================

// ===================================== HEADER ENDPOINTS =====================================
app.post('/alp/loginpost', qry_login.loginpost);
app.post('/alp/loginsessionset', qry_login.loginsessionset);
app.get('/alp/checksession', sessioncheck.auth, sessioncheck.authenticateToken, qry_login.checksession);
app.get('/alp/logoutpost', qry_login.logoutpost);
app.post('/alp/forgotpass', qry_login.forgotpass);
app.post('/alp/resetpass', qry_login.resetpass);
// ===================================== HEADER ENDPOINTS =====================================

// ===================================== FDW ENDPOINTS =====================================
app.post('/alp/searchBCS20', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.searchBCS20);
app.post('/alp/searchLISBCS20', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.searchLISBCS20);
app.post('/alp/searchBCS20batch', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.searchBCS20batch);
app.post('/alp/searchLISBCS20batch', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.searchLISBCS20batch);
app.post('/alp/searchSubHist', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.searchSubHist);
app.post('/alp/srchBCS1', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.srchBCS1);
app.post('/alp/srchBCS2', sessioncheck.auth, sessioncheck.authenticateToken, qry_fdw.srchBCS2);
// ===================================== FDW ENDPOINTS =====================================

// ===================================== USERS ENDPOINTS =====================================
app.get('/alp/alphaListingUsers', sessioncheck.auth, sessioncheck.authenticateToken, qry_users.alphaListingUsers);
app.post('/alp/insertUser', sessioncheck.auth, sessioncheck.authenticateToken, qry_users.insertUser);
app.post('/alp/updateUser', sessioncheck.auth, sessioncheck.authenticateToken, qry_users.updateUser);
app.post('/alp/updateEmail', sessioncheck.auth, sessioncheck.authenticateToken, qry_users.updateEmail);
app.post('/alp/updatePass', sessioncheck.auth, sessioncheck.authenticateToken, qry_users.updatePass);
// ===================================== USERS ENDPOINTS =====================================

// ===================================== RESOURCES ENDPOINTS =====================================
app.get('/alp/projDetails', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc.projDetails);
app.get('/alp/regOffc', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc.regOffc);
app.get('/alp/distOffc', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc.distOffc);
app.get('/alp/usrRegOffc', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc_alp.usrRegOffc);
app.get('/alp/usrDistOffc', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc_alp.usrDistOffc);
app.post('/alp/afpDbase', sessioncheck.auth, sessioncheck.authenticateToken, qry_rsrc_alp.afpDbase);
// ===================================== RESOURCES ENDPOINTS =====================================

// ===================================== RUN PORT =====================================
app.listen(process.env.PORT_RUNNER, () => {console.log("Server started at port "+process.env.PORT_RUNNER)})
module.exports = app;
// ===================================== RUN PORT =====================================