FROM node:18.0-slim
COPY . .
RUN npm install pm2 -g
CMD [ "pm2-runtime", "npm", "--", "start" ]
